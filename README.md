<h1 align="center">Sistem RGB</h1>
<h3 align="center">Proiect SMP 2021<br></h3>

## 📖 Cuprins
 - [Descriere](#descriere)
 - [Functionalitati](#functionalitati)
 - [Componente folosite](#componente)
 - [Resurse](#resurse)
 - [Modul de lucru](#mod)
 - [TODO](#TODO)
 - [Autor](#autor)

 ## 📝 Descriere <a name="descriere"></a>
 [Marketing] Te-ai saturat de monotonia biroului de acasa? Adauga-i lumina ambientala interactiva INSERT_NAME si bucura-te de un joc de lumini ce nu s-a mai vazut pana acum sau bucura-te de o lumina statica pe gustul tau care sa dea personalitate micului tau lacas.
 ## 🚀 Functionalitati <a name="functionalitati"></a>
  - [ ] joc de lumini
  - [ ] setare intensitate
  - [ ] aplicatie de control
  - [ ] ceas desteptator
  - [ ] joc de lumini in functie de sunet
 ## ⛏️ Componente folosite <a name="componente"></a>
  - [x] placuta Arduino UNO
  - [x] banda led RGB (de preferat sa nu fie ceva la 30lei ca e mare fail)
  - [x] telecomanda
  - [x] senzor/receptor infrarosu
  - [x] breadboard 
  - [x] fire de legatura (male & female)
  - [x] rezistente
  - [x] incarcator 12V 
  - [x] conector (mufa alimentare mama)
  - [x] buzzer
  - [x] buton
  - [x] modul ceas
  - [x] potentiometru reglabil
 ## 🆘 Resurse <a name="resurse"></a>
   - [Guide 1](https://www.youtube.com/watch?v=5M24QUVE0iU&ab_channel=MUO)
   - [Guide 2](https://www.youtube.com/watch?v=rPvGLSuMaLA&ab_channel=EssentialEngineering)
   - [Guide 3](https://www.youtube.com/watch?v=R102xfcx75I&ab_channel=AntonyCartwright)
   - [Guide 4](https://www.youtube.com/watch?v=NZuYO5WWsFk&ab_channel=BrainSmash)
   - [Loading more links](https://wwww.google.com)

 ## 📐 Modul de lucru <a name="mod"></a>
  O sa va prezint modul de lucru in cativa pasi simpli:
  1. Nu trebuie sa ai nicio idee ce faci.
  2. Loading...
 ## ☑️ TODO <a name="TODO"></a>
  - [x] sectiune pentru descrierea modului de lucru
  - [x] editare readme
  - [x] creare readme
  - [ ] finalizare proiect (daca mi-ar veni si piesele...)
  - [ ] adaugare cod sursa pentru jocul de lumini si pentru ceasul desteptator
  
 ## 👽 Autor <a name="autor"></a>
  - #### Udroiu Ruben

